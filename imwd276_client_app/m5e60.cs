﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace imwd276_client_app
{
    class Program
    {
        static int Main(string[] args)
        {
            #region Traitement Arguments
            // Console writeLine new line
            Console.WriteLine();
            // Attributs
            string commandIpAddress = "";
            bool oneOptionIsDefined = false;
            string listCommand = "" + Environment.NewLine +
                "******* Liste des commandes disponibles *******" + Environment.NewLine +
                "--target   :   Pointe l'adresse IP qui va suivre cette option (exemple: --target 10.10.10.10)" + Environment.NewLine;

            // Traitement arguments
            foreach (string oneArgument in args)
            {
                // Options
                if (oneArgument.Length > 2 && oneArgument.Substring(0, 2) == "--")
                {
                    switch (oneArgument)
                    {
                        case "--target":
                            Console.WriteLine("Option target choosed");
                            oneOptionIsDefined = true;
                            break;
                        default:
                            Console.WriteLine("Cette option n'existe pas");
                            Console.WriteLine(listCommand);
                            return 0;
                    }
                } 
                // Arguments user
                else
                {
                    commandIpAddress = oneArgument;
                }
            }

            // Check if target is defined
            if (!oneOptionIsDefined)
            {
                Console.WriteLine("Aucune option n'a été choisit.");
                Console.WriteLine(listCommand);
                return 0;
            }

            // Check target is IPV4
            IPAddress targetAddressFormat;
            if (IPAddress.TryParse(commandIpAddress, out targetAddressFormat))
            {
                if(targetAddressFormat.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    Console.WriteLine();
                    Console.WriteLine("L'adresse IPV4 renseignée n'est pas valide");
                    return 0;
                }
            } 
            else
            {
                Console.WriteLine();
                Console.WriteLine("L'adresse IPV4 renseignée n'est pas valide");
                return 0;
            }

            // Get current IP
            string myIpv4 = "";
            string myIpv6 = "";
            dynamic myIPs = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
            foreach(System.Net.IPAddress ipadress in myIPs)
            {
                IPAddress address;
                if (IPAddress.TryParse(ipadress.ToString(), out address))
                {
                    switch (address.AddressFamily)
                    {
                        case System.Net.Sockets.AddressFamily.InterNetwork:
                            // we have IPv4
                            myIpv4 = ipadress.ToString();
                            break;
                        case System.Net.Sockets.AddressFamily.InterNetworkV6:
                            // we have IPv6
                            myIpv6 = ipadress.ToString();
                            break;
                        default:
                            // umm... yeah... I'm going to need to take your red packet and...
                            break;
                    }
                }
            }
            #endregion

            #region Traitement JSON
            // Récupération de mon JSON
            string html = string.Empty;
            string url = @"http://" + commandIpAddress + "/displayed-messages";

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }
            }
            catch (InvalidCastException e)
            {
                Console.WriteLine("Erreur serveur : " + e.Message.ToString());
                return 0;
            }

            
            // String to JSON object
            System.Collections.Generic.List<Element> collectionElement = JsonConvert.DeserializeObject<System.Collections.Generic.List<Element>>(html);
            // Show json.title
            Console.WriteLine();
            Console.WriteLine("Bonjour, je suis IMWD276, actuelle sur " + myIpv4 + ", Je me connecte sur le serveur " + commandIpAddress + " et j'affiche une liste de messages :");
            // Foreach display messages
            foreach(Element oneElement in collectionElement)
            {
                string name = oneElement.nomMessage;
                string contenu = oneElement.contenuMessage.ToString();
                DateTime createdAt = Convert.ToDateTime(oneElement.created_at);
                /*DateTime updateAt = Convert.ToDateTime(oneElement.updated_at);*/
                Console.WriteLine("Nom: " + name + " Contenu: " + contenu + " (Créé le " + createdAt.Day + "/" + createdAt.Month + "/" + createdAt.Year + " à " + createdAt.Hour + ":" + createdAt.Minute + ")");

            }

            return 0;
            #endregion
        }
    }
}

