﻿using System;
using System.Collections.Generic;
using System.Text;

namespace imwd276_client_app
{
    class Element

    {
        public int id { get; set; }
        public string nomMessage { get; set; }
        public int contenuMessage { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }
}
